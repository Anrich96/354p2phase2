USE PersonDB;
CREATE TABLE Desserts
(
	DessertID int NOT NULL Primary KEY,
	DessertName varchar(255),
	Calories int,
);

CREATE TABLE Restaurant
(
	RestaurantID int NOT NULL PRIMARY KEY,
	RestaurantName varchar(255),
	Special varchar(255),
	FOREIGN KEY (RestaurantID) REFERENCES Persons (PersonID)
);