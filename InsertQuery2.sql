USE PersonDB;
INSERT INTO Desserts
(DessertID, DessertName, Calories)
VALUES
(1, 'Fruit Salad', 50),
(2, 'Chocolate Cake', 371),
(3, 'Chocolate Mousse', 225),
(4, 'Bow Tie', 119),
(5, 'White Rabbits', 20),
(6, 'Condensed Milk', 321);
INSERT INTO Restaurant
(RestaurantID, RestaurantName, Special)
VALUES
(1, 'Wimpy', 'Wimpy Salad'),
(2, 'Mugg and Bean', 'Coffee and Cake'),
(3, 'Turn n Tender', '1kg Rib Eye'),
(4, 'Kung Fu Kitchen', 'Cashew Chicken'),
(5, 'Ocean Basket', 'Sushi Platter for One'),
(6, 'Mc Donalds', 'Happy Meal');
